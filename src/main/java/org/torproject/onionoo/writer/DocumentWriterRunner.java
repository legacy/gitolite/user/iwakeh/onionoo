/* Copyright 2014--2017 The Tor Project
 * See LICENSE for licensing information */

package org.torproject.onionoo.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentWriterRunner {

  private static final Logger log = LoggerFactory.getLogger(
      DocumentWriterRunner.class);

  private DocumentWriter[] documentWriters;

  /** Instantiates a new document writer runner with newly created
   * instances of all known document writer implementations. */
  public DocumentWriterRunner() {
    SummaryDocumentWriter sdw = new SummaryDocumentWriter();
    DetailsDocumentWriter ddw = new DetailsDocumentWriter();
    BandwidthDocumentWriter bdw = new BandwidthDocumentWriter();
    WeightsDocumentWriter wdw = new WeightsDocumentWriter();
    ClientsDocumentWriter cdw = new ClientsDocumentWriter();
    UptimeDocumentWriter udw = new UptimeDocumentWriter();
    this.documentWriters = new DocumentWriter[] { sdw, ddw, bdw, wdw, cdw,
        udw };
  }

  /** Lets each configured document writer write its documents. */
  public void writeDocuments() {
    for (DocumentWriter dw : this.documentWriters) {
      log.debug("Writing " + dw.getClass().getSimpleName());
      dw.writeDocuments();
    }
  }

  /** Logs statistics of all configured document writers. */
  public void logStatistics() {
    for (DocumentWriter dw : this.documentWriters) {
      String statsString = dw.getStatsString();
      if (statsString != null) {
        log.info(dw.getClass().getSimpleName() + "\n"
            + statsString);
      }
    }
  }
}

